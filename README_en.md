[English](README_en.md) | [Русский](README.md)
# AutoVision

**An innovative product for automatic recognition of license plates, vehicle makes, and colors. Utilizes advanced image processing technologies, ensuring accurate and fast identification of vehicles on roads or in parking lots.**

##### Installation Guide

For a successful installation, Python version 3.6 or higher and Docker are required.

```bash
docker-compose up --build
```
Wait for the process to complete and the necessary AI models to be downloaded.

The running site will be accessible at http://0.0.0.0:4444/.

Create an admin account for access to the admin panel.


```bash 
docker exec -it  car_number-web_state_numbers-1 bash
```

```bash 
python manage.py createsuperuser
```

*** 

## Interface Guide
***Multiple photos can be selected***

## ![Пример изображения](docs/img.png)

***Approximate Result***

![img.png](docs/img2.png)
***
### AutoVision API

*Access to API*

All endpoints are available here http://0.0.0.0:4444/swagger/
* Authentication: JWT
* Bearer is mandatory for all requests
