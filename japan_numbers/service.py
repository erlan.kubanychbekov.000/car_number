import io
from django.core.files.base import ContentFile
from japan_numbers.models import JapaneseCar
from japan_numbers.ai.main import japanese_numbers
from django.db import transaction


@transaction.atomic()
def create_car_by_image_japanese(images, get_models=False, user=None, log_message='web'):
    car_list = []
    for image in images:
        uploaded = JapaneseCar.objects.create(image=image)
        score, lp = japanese_numbers(uploaded.image.path)
        image_io = io.BytesIO()

        image_file = ContentFile(image_io.getvalue(), 'temp.jpg')
        uploaded.car_number = lp
        uploaded.score = score
        uploaded.country = "Japan"
        uploaded.car_number_photo.save(f'cur_number{uploaded.id}.jpg', image_file)
        uploaded.save()
        if not get_models:
            car_list.append(uploaded.id)
        else:
            car_list.append(uploaded)
    return car_list