from django.urls import path

from japan_numbers.web.views import process_image_japanese


urlpatterns = [
    path('japan/', process_image_japanese, name='process_image_japan'),
]