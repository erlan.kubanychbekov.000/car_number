from django.apps import AppConfig


class JapanNumbersConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'japan_numbers'
