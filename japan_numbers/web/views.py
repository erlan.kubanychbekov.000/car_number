from django.shortcuts import render
from urllib.parse import urlencode
from django.shortcuts import render, redirect
from state_numbers.models import Car
from japan_numbers.service import create_car_by_image_japanese
from django.contrib.auth import authenticate, login


def process_image_japanese(request):
    if request.user.is_authenticated:
        if request.method == 'POST':
            images = request.FILES.getlist('image')
            try:
                car_list = create_car_by_image_japanese(images)
                return redirect(f'/list/?{urlencode({"id": car_list}, True)}')
            except Exception as e:
                return render(request, 'japan_numbers/japanese.html',
                              {'error': 'Возникла, какая-то ошибка'})
        return render(request, 'japan_numbers/japanese.html')

    return redirect('login')