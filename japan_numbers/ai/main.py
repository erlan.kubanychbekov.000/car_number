from paddleocr import PaddleOCR

def read_lp(image_path):
    ocr = PaddleOCR(
        use_angle_cls=True,
        lang='japan',
        rec_model_dir='japan_numbers/ai/models/japan_PP-OCRv3_rec_infer',
    )

    try:
        results = ocr.ocr(image_path, cls=True)

        for line in results:
            scores = [i[-1][1] for i in line]
            chars = ''.join([i[-1][0] for i in line])

            avg_score = sum(scores) / len(scores) if scores else 0
            return avg_score, chars

    except TypeError as e:
        return 0, "Not detected!"

def japanese_numbers(image_path):
    
    # score and detected lisence plate
    score, lp = read_lp(image_path)
    print(score, lp)
    return score, lp

# japanese_numbers(image_path="a.jpeg")