from django.contrib import admin
from simple_history.admin import SimpleHistoryAdmin

from japan_numbers.models import JapaneseCar


admin.site.register(JapaneseCar, SimpleHistoryAdmin)