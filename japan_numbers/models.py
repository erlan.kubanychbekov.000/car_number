from django.db import models
from simple_history.models import HistoricalRecords

from django.contrib.admin.models import LogEntry, ADDITION
from django.contrib.contenttypes.models import ContentType


class JapaneseCar(models.Model):
    image = models.ImageField(upload_to='uploads/cars/', null=True, blank=True)
    car_number = models.CharField(max_length=10, verbose_name="Номер машины")
    car_number_photo = models.ImageField(upload_to='uploads/car_numbers/', null=True, blank=True)
    country = models.CharField(max_length=25, null=True)
    score = models.CharField(max_length=255, null=True, blank=True, verbose_name="Точность")
    history = HistoricalRecords()

    def __str__(self):
        return f"Image {self.car_number}"

    def save(self, *args, **kwargs):
        user = kwargs.pop('user', None)
        log_entry = kwargs.pop('log_entry', False)
        change_message = kwargs.pop('change_message', 'web')
        super(JapaneseCar, self).save(*args, **kwargs)
        if log_entry:
            LogEntry.objects.create(
                content_type_id=ContentType.objects.get_for_model(self).pk,
                object_id=self.pk,
                object_repr=str(self),
                action_flag=ADDITION,
                user=user,
                change_message=change_message
            )
