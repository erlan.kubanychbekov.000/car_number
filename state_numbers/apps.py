from django.apps import AppConfig


class GosNomeraConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'state_numbers'
