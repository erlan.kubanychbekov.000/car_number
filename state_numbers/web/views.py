from urllib.parse import urlencode
from django.shortcuts import render, redirect
from state_numbers.models import Car
from state_numbers.service import create_car_by_image
from django.contrib.auth import authenticate, login


def sign_in(request):
    if request.method == 'POST':
        username = request.POST["username"]
        password = request.POST["password"]
        user = authenticate(request, username=username, password=password)
        if user is not None:
            login(request, user)
            return redirect('/')
    return render(request, 'state_numbers/login.html')


def car_number(request, image_id):
    image = Car.objects.get(id=image_id)
    return render(request, 'state_numbers/result.html', {'image': image})


def process_image(request):
    if request.user.is_authenticated:
        if request.method == 'POST':
            images = request.FILES.getlist('image')
            try:
                car_list = create_car_by_image(images, user=request.user)
                return redirect(f'list/?{urlencode({"id": car_list}, True)}')
            except Exception as e:
                return render(request, 'state_numbers/main.html',
                              {'error': 'Возникла, какая-то ошибка'})
        return render(request, 'state_numbers/main.html')
    return redirect('login')


def list_cars(request):
    car_list = Car.objects.all()
    car_ids = request.GET.getlist('id')
    if car_list:
        car_list = car_list.filter(id__in=car_ids)
    return render(request, 'state_numbers/list.html', {'car_list': car_list})
