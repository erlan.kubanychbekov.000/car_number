from simple_history.admin import SimpleHistoryAdmin
from django.contrib import admin
from .models import Car

from django.contrib.admin.models import LogEntry
from django.urls import reverse
from django.utils.html import format_html


@admin.register(LogEntry)
class LogEntryAdmin(admin.ModelAdmin):
    date_hierarchy = 'action_time'

    list_filter = [
        'user',
        'content_type',
        'action_flag'
    ]

    search_fields = [
        'object_repr',
        'change_message'
    ]

    list_display = [
        'action_time',
        'view_object_link',
        'user',
        'content_type',
        'action_flag',
    ]

    def view_object_link(self, obj):
        if obj.content_type and obj.object_id:
            url = reverse(f"admin:{obj.content_type.app_label}_{obj.content_type.model}_change", args=[obj.object_id])
            return format_html('<a href="{}">{}</a>', url, obj.object_repr)
        return obj.object_repr

    view_object_link.short_description = 'Object'


admin.site.register(Car, SimpleHistoryAdmin)
