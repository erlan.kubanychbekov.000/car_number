import io
from django.core.files.base import ContentFile
from .models import Car
from state_numbers.ai.image_processing import lp_det_reco
from django.db import transaction


@transaction.atomic()
def create_car_by_image(images, get_models=False, user=None, log_message='web'):
    car_list = []
    for image in images:
        uploaded = Car.objects.create(image=image)
        number, pil_image, number_conf, country, country_conf = lp_det_reco(uploaded.image.path)
        image_io = io.BytesIO()
        pil_image.save(image_io, format='JPEG')

        image_file = ContentFile(image_io.getvalue(), 'temp.jpg')
        uploaded.car_number = number
        uploaded.processed_text = number_conf
        uploaded.country = country
        uploaded.car_number_photo.save(f'cur_number{uploaded.id}.jpg', image_file)
        uploaded.save(log_entry=True, user=user, change_message=log_message)
        if not get_models:
            car_list.append(uploaded.id)
        else:
            car_list.append(uploaded)
    return car_list

