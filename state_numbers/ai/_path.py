import os
import sys

current_dir = os.path.dirname(os.path.abspath(__file__))
nomeroff_net_dir = os.path.join(current_dir, "nomeroff_net")
sys.path.append(nomeroff_net_dir)