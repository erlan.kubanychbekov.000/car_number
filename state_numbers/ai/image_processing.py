import torch
import os
import cv2
import zipfile
import numpy as np
from PIL import Image
from glob import glob
from fastai.learner import load_learner
from basicsr.archs.rrdbnet_arch import RRDBNet
from basicsr.utils.download_util import load_file_from_url
from state_numbers.ai.realesrgan import RealESRGANer
from skimage.feature import canny
from skimage.transform import hough_line, hough_line_peaks
from skimage.transform import rotate
from skimage.color import rgb2gray
from ._path import nomeroff_net_dir
from .nomeroff_net.nomeroff_net import pipeline
from .nomeroff_net.nomeroff_net.tools import unzip
from .strhub.data.module import SceneTextDataModule
from paddleocr import PaddleOCR

country_model = 'country_model_v7am.pk1'
file_url = ['https://cloud.sanarip.org/index.php/s/TbMfZgdDrbRLe7g/download/country_model_v7am.pk1']
model_name = 'RealESRGAN_x4plus'
model_url = ['https://cloud.sanarip.org/index.php/s/2XfFdM4CJt52rmm/download/RealESRGAN_x4plus.pth']

# Initialize OCR models and other resources
ocr = PaddleOCR(use_angle_cls=True, lang="en")
parseq = torch.hub.load('baudm/parseq', 'parseq', pretrained=True).eval()
img_transform = SceneTextDataModule.get_transform(parseq.hparams.img_size)


# Download models if they don't exist
def download_model(model_url, model_name):
    model_path = os.path.join('weights', model_name + '.pt')
    if not os.path.isfile(model_path):
        ROOT_DIR = os.path.dirname(os.path.abspath(__file__))
        for url in model_url:
            model_path = load_file_from_url(
                url=url, model_dir=os.path.join(ROOT_DIR, 'weights'), progress=True, file_name=None)
    return model_path


country_model_path = download_model(file_url, country_model)
real_esrgan_model_path = download_model(model_url, model_name)

data_path = os.path.join('.', 'data', 'models', 'Detector', 'yolov8', 'yolov8s-2023-02-11.pt')
if not os.path.isfile(data_path):
    ROOT_DIR = os.path.dirname(os.path.abspath(__file__))
    data_path = load_file_from_url(
        url='https://cloud.sanarip.org/index.php/s/NYpfBa3BSot7APe/download/data.zip', model_dir=os.path.join(ROOT_DIR),
        progress=True, file_name=None)



delete_data = 'data.zip'
if os.path.isfile(delete_data):
    with zipfile.ZipFile('data.zip', 'r') as zip_ref:
        zip_ref.extractall()
if os.path.isfile(delete_data):
    os.remove(delete_data)

multiline_number_plate_detection_and_reading = pipeline("multiline_number_plate_detection_and_reading",
                                                        image_loader="opencv")

# Create an instance of the RealESRGANer class
model_RDB = RRDBNet(num_in_ch=3, num_out_ch=3, num_feat=64, num_block=23, num_grow_ch=32, scale=4)
upsampler = RealESRGANer(
    scale=4,
    model_path=real_esrgan_model_path,
    dni_weight=None,
    model=model_RDB,
    tile=0,
    tile_pad=10,
    pre_pad=0,
    half=False,
    gpu_id=None)

# Country model
country_model = load_learner(country_model_path)


# Define a function for preprocessing an image
def preprocess(img):
    # Detect rotation angle
    rot_angle = 0
    grayscale = rgb2gray(img)
    edges = canny(grayscale, sigma=3.0)
    out, angles, distances = hough_line(edges)
    _, angles_peaks, _ = hough_line_peaks(out, angles, distances, num_peaks=20)
    angle = np.mean(np.rad2deg(angles_peaks))

    # Adjust rotation angle
    if 0 <= angle <= 90:
        rot_angle = angle - 90
    elif -45 <= angle < 0:
        rot_angle = angle - 90
    elif -90 <= angle < -45:
        rot_angle = 90 + angle
    if abs(rot_angle) > 20:
        rot_angle = 0

    # Rotate and crop the image
    rotated = rotate(img, rot_angle, resize=True) * 255
    rotated = rotated.astype(np.uint8)
    rotated1 = rotated[:, :, :]
    minus = np.abs(int(np.sin(np.radians(rot_angle)) * rotated.shape[0]))
    if rotated.shape[1] / rotated.shape[0] < 3 and minus > 6:
        rotated1 = rotated[minus:-minus, :, :]
    return rotated1


# Define a function to remove unwanted characters from a string
def del_symbols(input_string):
    cleaned_string = ''
    for char in input_string:
        if char not in (
                '-', '#', '_', '+', '=', '!', '@', '$', '%', '*', '&', '(', ')', '^', '/', '|', ';', ':', '.', ',', '·',
                "<",
                ">"):
            cleaned_string += char

    return cleaned_string


# PaddleOCR takes cv2 image
def paddle(img, lang):
    recognition = PaddleOCR(use_angle_cls=True, lang=lang, det=False)
    result = recognition.ocr(img)
    results_array = []
    confidences_array = []
    for idx in range(len(result[0])):
        res = result[0][idx][1][0]
        confidence = result[0][idx][1][1]
        results_array.append(res)
        confidences_array.append(confidence)
    combined_element = ''.join(results_array)
    final = combined_element.replace(' ', '')

    # returns text and confidential probability
    return final, confidences_array


# Parseq OCR takes PIL image
def parseq_finc(img):
    img = img_transform(img).unsqueeze(0)
    logits = parseq(img)
    pred = logits.softmax(-1)
    label, confidence = parseq.tokenizer.decode(pred)

    return label[0], confidence


def calculate_mean(numbers):
    if len(numbers) == 0:
        return None  # Handle the case of an empty array

    total = sum(numbers)
    mean = total / len(numbers)
    return mean


def lp_det_reco(img_path):
    result = multiline_number_plate_detection_and_reading(glob(img_path), matplotlib_show=True)
    (images, images_bboxs,
     images_points, images_zones, region_ids,
     region_names, count_lines,
     confidences, texts,) = unzip(result)

    # get bounding box of the segmented number plate
    x_min, y_min, x_max, y_max, _, _ = images_bboxs[0][0]
    x_min, y_min, x_max, y_max = int(x_min), int(y_min), int(x_max), int(y_max)
    pro = preprocess(images[0][y_min:y_max, x_min:x_max])
    if count_lines[0][0] == 1:
        pro = images_zones[0][0].astype('uint8')

    # country
    country_values = country_model.predict(pro)
    country = country_values[0]
    country_conf = country_values[2].detach().numpy()[len(country_values[2].detach().numpy()) - 1]
    if country_conf > 1.0:
        country_conf = 1.0

    # enchance img or not
    H, W, _ = pro.shape
    if H >= 100 and H <= 300 and W >= 100 and W <= 300:
        img_enh, _ = upsampler.enhance(pro, outscale=1)
    else:
        img_enh = pro
    img_final = Image.fromarray(img_enh)
    H, W, _ = img_enh.shape
    # OCR
    match country:
        # OCR if this is Chinese license plate
        case "CN":
            combined_element_without_spaces, conf = paddle(img_enh, 'ch')
            combined_element_without_spaces = del_symbols(combined_element_without_spaces)
            if combined_element_without_spaces[0] == '0' or combined_element_without_spaces[1] == '0':
                combined_element_without_spaces = combined_element_without_spaces.replace('0', 'Q', 1)

        # OCR if this is Kyrgyz license plate
        case 'AM':
            # Use paddleOCR if squared number plate
            if count_lines[0][0] >= 2:
                square_w = int(W / 2.7)  # Adjust the size as needed
                square_h = int(H / 2)
                black_square = np.zeros((square_h, square_w, 3), dtype=np.uint8)
                img_enh[(-square_h):, :square_w] = black_square
                combined_element_without_spaces, conf = paddle(img_enh, 'en')
                combined_element_without_spaces = del_symbols(combined_element_without_spaces).replace('KG', '')
            else:
                # Takes values from nomeroff-net
                conf = confidences[0][0]
                combined_element_without_spaces = texts[0][0]
        case 'KG':
            # Use paddleOCR if squared number plate
            if count_lines[0][0] >= 2:
                square_w = int(W / 2.7)  # Adjust the size as needed
                square_h = int(H / 2)
                black_square = np.zeros((square_h, square_w, 3), dtype=np.uint8)
                img_enh[(-square_h):, :square_w] = black_square
                combined_element_without_spaces, conf = paddle(img_enh, 'en')
                combined_element_without_spaces = del_symbols(combined_element_without_spaces).replace('KG', '')
                Kg_new_put = [i for i in combined_element_without_spaces]
                try:
                    if Kg_new_put[0] == '0' and type(int(Kg_new_put[1])) == int and len(Kg_new_put) >= 7 and Kg_new_put[
                        1] != '0':
                        Kg_new_put.insert(2, 'KG')
                except:
                    pass
                combined_element_without_spaces = Kg_new_put
            else:
                # Takes values from nomeroff-net
                proo = pro.copy()
                H, W, _ = proo.shape
                square_w = int(W / 3.5)  # Adjust the size as needed
                square_h = int(H / 3.5)
                black_square = np.zeros((square_h, square_w, 3), dtype=np.uint8)
                proo[(-square_h):, :square_w] = black_square
                combined_element_without_spaces, conf = paddle(proo, 'en')
                Kg_new_put = [i for i in combined_element_without_spaces]
                try:
                    if Kg_new_put[0] == '0' and type(int(Kg_new_put[1])) == int and len(Kg_new_put) >= 7 and Kg_new_put[
                        1] != '0':
                        Kg_new_put.insert(2, 'KG')
                except:
                    pass

                combined_element_without_spaces = ''.join(Kg_new_put[:])

        # OCR if this is Russian and Kazakhstan license plates
        case 'RU' | 'KZ':
            # Use paddleOCR if squared number plate
            if count_lines[0][0] >= 2:
                combined_element_without_spaces, conf = paddle(img_enh, 'en')
                combined_element_without_spaces = combined_element_without_spaces.replace('KZ', '')
            else:
                # Takes values from nomeroff-net
                conf = confidences[0][0]
                combined_element_without_spaces = texts[0][0]

        case 'UZ':
            # Use paddleOCR if squared number plate
            if count_lines[0][0] >= 2:
                combined_element_without_spaces, conf = paddle(img_enh, 'en')
            else:
                # Number plates divide into 2 parts and use pareq
                start = img_enh[:, :int(W // 4.5)]
                end = img_enh[:, int(W // 4.5):]
                number_plate = [start, end]
                result = []
                conf = []
                for i in range(len(number_plate)):
                    label, confidence = parseq_finc(Image.fromarray(number_plate[i]))
                    for j in range(len(confidence[0])):
                        confidence_var = confidence[0].detach().numpy()[j]
                    conf.append(confidence_var)
                    result.append(label)
                combined_element = ''.join(result)
                combined_element_without_spaces = combined_element.replace('G', '0', 2)

        # OCR if this is other country license plates
        case _:
            # Use paddleOCR if squared number plate
            if count_lines[0][0] >= 2:
                combined_element_without_spaces, conf = paddle(img_enh, 'en')
            else:
                # Use pareq
                image_final = Image.fromarray(img_enh)
                label, confidence = parseq_finc(image_final)
                conf = []
                for i in range(len(confidence[0])):
                    confidence_var = confidence[0].detach().numpy()[i]
                    conf.append(confidence_var)
                combined_element_without_spaces = label

    return del_symbols(combined_element_without_spaces), img_final, calculate_mean(conf), country, country_conf
