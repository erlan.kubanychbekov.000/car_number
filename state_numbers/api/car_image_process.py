from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.parsers import MultiPartParser

from drf_yasg.utils import swagger_auto_schema
from drf_yasg import openapi

from state_numbers.service import create_car_by_image
from .serilazers import CarSerializers
from rest_framework.permissions import IsAuthenticated, AllowAny
from rest_framework import status

from django.contrib.admin.models import LogEntry, ADDITION
from django.contrib.contenttypes.models import ContentType


class CarPhotoProcessing(APIView):
    """
    Works only with authorization, the token must be sent to the header in Authorization: your token.
    The token is valid for 1 hour, after expiration you need to update the token via refresh token
    """
    parser_classes = (MultiPartParser,)
    serializer_class = CarSerializers
    permission_classes = (IsAuthenticated,)

    @swagger_auto_schema(
        manual_parameters=[
            openapi.Parameter(
                'image',
                openapi.IN_FORM,
                description='The image file to be uploaded',
                type=openapi.TYPE_FILE,
            ),
        ],
        responses={200: CarSerializers(many=True), 400: "Bad request"},
    )
    def post(self, request, *args, **kwargs):
        images = request.data.getlist('image')
        try:
            if images:
                car_list = create_car_by_image(images, get_models=True, user=request.user, log_message='api')
                serializers = self.serializer_class(car_list, many=True, read_only=True)
                return Response(serializers.data)
            return Response(status=status.HTTP_400_BAD_REQUEST)

        except Exception as e:
            return Response(status=status.HTTP_400_BAD_REQUEST)
