from rest_framework import serializers
from state_numbers.models import Car


class CarSerializers(serializers.ModelSerializer):
    class Meta:
        model = Car
        fields = '__all__'
