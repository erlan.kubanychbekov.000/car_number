from .web import views
from django.conf import settings
from django.conf.urls.static import static
from state_numbers.api.car_image_process import CarPhotoProcessing
from django.urls import path
from rest_framework_simplejwt import views as jwt_views

urlpatterns = [
    path('', views.process_image, name='process_image'),
    path('list/', views.list_cars, name='list'),
    path('login/', views.sign_in, name='login'),
    path('result/<int:image_id>/', views.car_number, name='result'),
    path('api/process-image/', CarPhotoProcessing.as_view(), name='api-process-image'),
    path('api/token/', jwt_views.TokenObtainPairView.as_view(), name='token_obtain_pair'),
    path('api/token/refresh/', jwt_views.TokenRefreshView.as_view(), name='token_refresh'),
]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
